﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSounds : MonoBehaviour
{
    public static MenuSounds instance;
    
    [SerializeField] public AudioClip[] playButtonSounds;
    [SerializeField] public AudioClip[] purchaseButtonSounds;

    private void Awake()
    {
        MakeSingleton();
    }
    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Update()
    {
        if (MenuController.instance.playGamePressed)
        {
            PlayButtonSound();
            MenuController.instance.playGamePressed = false;
        }
        if (MenuController.instance.shopButtonPressed)
        {
            PurchaseButtonSound();
            MenuController.instance.shopButtonPressed = false;
        }
    }

    public void PlayButtonSound()
    {
        int randomNumber = Random.Range(0, playButtonSounds.Length);
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = playButtonSounds[randomNumber];
        source.Play();
        Destroy(source, playButtonSounds[randomNumber].length);
    }

    public void PurchaseButtonSound()
    {
        int randomNumber = Random.Range(0, purchaseButtonSounds.Length);
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = purchaseButtonSounds[randomNumber];
        source.Play();
        Destroy(source, purchaseButtonSounds[randomNumber].length);
    }
}
