﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeCollector : MonoBehaviour
{

    private GameObject[] pipeHolders;
    private float distance = 3.2f;
    private float lastPipeX;
    private float pipeMin = -1.5f;
    private float pipeMax = 2.4f;

    private void Awake()
    {
        pipeHolders = GameObject.FindGameObjectsWithTag("PipeHolder");

        for (int i = 0; i < pipeHolders.Length; i++)
        {
            Vector3 temp = pipeHolders[i].transform.position;
            temp.y = Random.Range(pipeMin, pipeMax);
            pipeHolders[i].transform.position = temp;
        }

        lastPipeX = pipeHolders[0].transform.position.x;

        for (int i = 1; i < pipeHolders.Length; i++)
        {
            if (lastPipeX < pipeHolders[i].transform.position.x)
            {
                lastPipeX = pipeHolders[i].transform.position.x;
            }
        }
    }

    private void Update()
    {
        if (BirdScript.instance != null)
        {
            if (BirdScript.instance.score < 26)
            {
                distance = 3.2f;
            }
            else if (BirdScript.instance.score > 25 && BirdScript.instance.score < 51)
            {
                distance = 3f;
            }
            else if (BirdScript.instance.score > 50 && BirdScript.instance.score < 101)
            {
                distance = 2.9f;
            }
            else if (BirdScript.instance.score > 100 && BirdScript.instance.score < 126)
            {
                distance = 2.8f;
            }
            else if (BirdScript.instance.score > 125 && BirdScript.instance.score < 151)
            {
                distance = 2.7f;
            }
            else if (BirdScript.instance.score > 150 && BirdScript.instance.score < 201)
            {
                distance = 2.6f;
            }
            else
            {
                distance = 2.5f;
            }
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PipeHolder")
        {
            Vector3 temp = collision.transform.position;

            temp.x = lastPipeX + distance;
            temp.y = Random.Range(pipeMin, pipeMax);

            collision.transform.position = temp;

            lastPipeX = temp.x;
        }
    }

}
