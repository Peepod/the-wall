﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BirdScript : MonoBehaviour
{

    public static BirdScript instance;


    [SerializeField]
    private Rigidbody2D myRigidBody;
    [SerializeField]
    private Animator anim;
    public AudioClip[] clips;
    private float forwardSpeed = 3f;

    private float bounceSpeed = 4f;
    private bool didFlap;
    public bool isAlive;
    private Button flapButton;
    public int lastPlayedSound;

    
    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip flapClick, pointClip, diedClip;
    public int score;

    

    private void Awake()
    {
        Debug.Log("Hello I am AWAKE");
        if (instance == null)
        {
            instance = this;
        }
        isAlive = true;
        score = 0;

        flapButton = GameObject.FindGameObjectWithTag("FlapButton").GetComponent<Button>();
        flapButton.onClick.AddListener(() => FlapTheBird());

        SetCamerasX();
    }

    void Start()
    {
        
    }

    void FixedUpdate()
    {
        if (isAlive)
        {
            Vector3 temp = transform.position;
            temp.x += forwardSpeed * Time.deltaTime;
            transform.position = temp;

            if(didFlap)
            {
                didFlap = false;
                myRigidBody.velocity = new Vector2(0, bounceSpeed);
                audioSource.PlayOneShot(flapClick);
                anim.SetTrigger("IsFlapping");
            }

            if (myRigidBody.velocity.y >= 0)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            } else
            {
                float angle = 0;
                angle = Mathf.Lerp(0, -90, -myRigidBody.velocity.y / 20);
                transform.rotation = Quaternion.Euler(0, 0, angle);
            }
        }
    }

    void SetCamerasX()
    {
        CameraScript.offsetX = (Camera.main.transform.position.x - transform.position.x) - 1f;
    }

    public void FlapTheBird()
    {
        didFlap = true;
    }

    public float GetPositionX()
    {
        return transform.position.x;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground" || collision.gameObject.tag == "Pipe")
        {
            if(isAlive)
            {
                isAlive = false;
                anim.SetTrigger("IsDead");
                audioSource.PlayOneShot(diedClip);
                PlayServices.instance.AddScoreToLeaderboard("CgkIqaDvsrYIEAIQBA", GameController.instance.getBricks());
                if (score < 100)
                {
                    GameController.instance.setBricks(score / 25);
                } else if ( score > 99 && score < 200)
                {
                    GameController.instance.setBricks(score / 20);
                } else 
                {
                    GameController.instance.setBricks(score / 10);
                }
                PlayServices.instance.SaveData();
                GameplayController.instance.PlayerDiedShowScore(score);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "PipeHolder")
        {
            score++;
            GameplayController.instance.SetScore(score);
            PlaySound();
        }
    }

    public int PlaySound()
    {
        int randomNumber = Random.Range(0, clips.Length);
        while (randomNumber == lastPlayedSound)
        {
            randomNumber = Random.Range(0, clips.Length);
        }
        lastPlayedSound = randomNumber;
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = clips[randomNumber];
        source.Play();
        Destroy(source, clips[randomNumber].length);
        return lastPlayedSound;
    }
}
