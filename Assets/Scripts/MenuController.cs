﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{

    public static MenuController instance;

    [SerializeField]
    private GameObject[] birds;
    private bool isGreenBirdUnlocked, isRedBirdUnlocked;
    public bool playGamePressed = false;
    public bool shopButtonPressed = false;
    [SerializeField] private Button shopButton;
    [SerializeField] private GameObject shopPanel;
    [SerializeField] public Text bricksOwned;
    [SerializeField] private Button brickExplained;
    [SerializeField] private GameObject brickInfo;

    private void Awake()
    {
        MakeInstance();
    }

    void Start()
    {
        birds[GameController.instance.GetSelectedBird()].SetActive(true);
        CheckIfBirdsAreUnlocked();
    }

       
    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    void CheckIfBirdsAreUnlocked()
    {
        if(GameController.instance.IsRedBirdUnlocked() ==1)
        {
            isRedBirdUnlocked = true;
        }
        if (GameController.instance.IsGreenBirdUnlocked() == 1)
        {
            isGreenBirdUnlocked = true;
        }
    }

    public void PlayGame()
    {
        SceneFader.instance.FadeIn("Gameplay");
        playGamePressed = true;
    }

    public void ChangeBird()
    {
        if(GameController.instance.GetSelectedBird() == 0) {
            if(isGreenBirdUnlocked)
            {
                birds[0].SetActive(false);
                GameController.instance.SetSelectedBird(1);
                birds[GameController.instance.GetSelectedBird()].SetActive(true);
            }
        } else if (GameController.instance.GetSelectedBird() == 1)
        {
            if(isRedBirdUnlocked)
            {
                birds[1].SetActive(false);
                GameController.instance.SetSelectedBird(2);
                birds[GameController.instance.GetSelectedBird()].SetActive(true);
            } else
            {
                birds[1].SetActive(false);
                GameController.instance.SetSelectedBird(0);
                birds[GameController.instance.GetSelectedBird()].SetActive(true);
            }
        } else if (GameController.instance.GetSelectedBird() == 2)
        {
            birds[2].SetActive(false);
            GameController.instance.SetSelectedBird(0);
            birds[GameController.instance.GetSelectedBird()].SetActive(true);
        }
    }
    
    public void ShowLeaderboard()
    {
        PlayServices.instance.ShowLeaderboardsUI();
    }

    public void ShowAchievements()
    {
        PlayServices.instance.ShowAchievementsUI();
    }

    public void OpenShop()
    {
        shopPanel.SetActive(true);
        shopButton.onClick.RemoveAllListeners();
        shopButton.onClick.AddListener(() => CloseShop());
    }
    
    public void CloseShop()
    {
        shopPanel.SetActive(false);
        shopButton.onClick.RemoveAllListeners();
        shopButton.onClick.AddListener(() => OpenShop());
    }

    public void BuyBrickItme()
    {
        IAPManager.instance.BuyBrick();
        shopButtonPressed = true;
    }

    public void OpenBrickExplained()
    {
        brickInfo.SetActive(true);
        brickExplained.onClick.RemoveAllListeners();
        brickExplained.onClick.AddListener(() => CloseBrickExplained());
    }

    public void CloseBrickExplained()
    {
        brickInfo.SetActive(false);
        brickExplained.onClick.RemoveAllListeners();
        brickExplained.onClick.AddListener(() => OpenBrickExplained());
    }

}
