﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public static GameController instance;
    private const string HIGH_SCORE = "High Score";
    private const string SELECTED_BIRD = "Selected Bird";
    private const string GREEN_BIRD = "Green Bird";
    private const string RED_BIRD = "Red Bird";
    //public string playerBricks = "Bricks Owned";
    public static int bricksInt;

    private void Awake()
    {
        MakeSingleton();
        IsTheGameStartedForTheFirstTime();
        //PlayerPrefs.DeleteAll();
        Debug.Log("BIRDW The has key value is: " + PlayerPrefs.GetInt("IsTheGameStartedForTheFirstTime"));
        Debug.Log("BIRDW The selected bird value is " + SELECTED_BIRD);
        Debug.Log("BIRDW The green bird value is " + GREEN_BIRD);
        Debug.Log("BIRDW The value of getselected bird is " + GetSelectedBird());
        Debug.Log("BIRDW the value of is green bird unlocked is " + IsGreenBirdUnlocked());
    }

    public void Update()
    {
        if (MenuController.instance.bricksOwned != null)
        {
            MenuController.instance.bricksOwned.text = "" + bricksInt;
        }
    }

    void MakeSingleton()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        } else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void IsTheGameStartedForTheFirstTime()
    {
        if(!PlayerPrefs.HasKey("IsTheGameStartedForTheFirstTime"))
        {
            Debug.Log("First Time Method Ran!");
            PlayerPrefs.SetInt(HIGH_SCORE, 0);
            PlayerPrefs.SetInt(SELECTED_BIRD, 0);
            PlayerPrefs.SetInt(GREEN_BIRD, 0);
            PlayerPrefs.SetInt(RED_BIRD, 0);
            PlayerPrefs.SetInt("IsTheGameStartedForTheFirstTime", 0);
        }
    }

    public void SetHighscore(int score)
    {
        PlayerPrefs.SetInt(HIGH_SCORE, score);
    }

    public int GetHighscore()
    {
        return PlayerPrefs.GetInt(HIGH_SCORE);
    }

    public void SetSelectedBird(int selectedBird)
    {
        PlayerPrefs.SetInt(SELECTED_BIRD, selectedBird);
    }

    public int GetSelectedBird()
    {
        return PlayerPrefs.GetInt(SELECTED_BIRD);
    }

    public void UnlockGreenBird()
    {
        PlayerPrefs.SetInt(GREEN_BIRD, 1);
    }

    public int IsGreenBirdUnlocked()
    {
        return PlayerPrefs.GetInt(GREEN_BIRD);
    }

    public void UnlockRedBird()
    {
        PlayerPrefs.SetInt(RED_BIRD, 1);
    }

    public int IsRedBirdUnlocked()
    {
        return PlayerPrefs.GetInt(RED_BIRD);
    }



    public void setBricks(int bricks)
    {
//        bricksInt = PlayerPrefs.GetInt(playerBricks);
        bricksInt = bricks + bricksInt;
        // PlayerPrefs.SetInt(playerBricks, bricksInt);
    }

    public int getBricks()
    {
        return bricksInt;
        //return PlayerPrefs.GetInt(playerBricks);
    }

 }
